/*
  Warnings:

  - You are about to drop the column `history` on the `character` table. All the data in the column will be lost.
  - Added the required column `story` to the `Character` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `character` DROP COLUMN `history`,
    ADD COLUMN `story` VARCHAR(191) NOT NULL;
