/*
  Warnings:

  - You are about to drop the column `edad` on the `character` table. All the data in the column will be lost.
  - You are about to drop the column `historia` on the `character` table. All the data in the column will be lost.
  - You are about to drop the column `nombre` on the `character` table. All the data in the column will be lost.
  - You are about to drop the column `peso` on the `character` table. All the data in the column will be lost.
  - Added the required column `age` to the `Character` table without a default value. This is not possible if the table is not empty.
  - Added the required column `history` to the `Character` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `Character` table without a default value. This is not possible if the table is not empty.
  - Added the required column `updatedAt` to the `Character` table without a default value. This is not possible if the table is not empty.
  - Added the required column `weight` to the `Character` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `character` DROP COLUMN `edad`,
    DROP COLUMN `historia`,
    DROP COLUMN `nombre`,
    DROP COLUMN `peso`,
    ADD COLUMN `age` INTEGER NOT NULL,
    ADD COLUMN `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    ADD COLUMN `deletedAt` DATETIME(3) NULL,
    ADD COLUMN `history` VARCHAR(191) NOT NULL,
    ADD COLUMN `name` VARCHAR(191) NOT NULL,
    ADD COLUMN `updatedAt` DATETIME(3) NOT NULL,
    ADD COLUMN `weight` DOUBLE NOT NULL;
