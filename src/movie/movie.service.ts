import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateMovieDto } from './dto/movie.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { ApiTags } from '@nestjs/swagger';
import { MovieWithCharacters, computeMovieYear } from './types/movie.type';
import { Prisma } from '@prisma/client';

@Injectable()
export class MovieService {
  constructor(private prismaService: PrismaService) {}

  @ApiTags('Movie')
  async getOne(id: number): Promise<MovieWithCharacters> {
    const movie = await computeMovieYear.movie.findUnique({
      where: {
        id: id,
      },
      select: {
        id: true,
        title: true,
        image: true,
        year: true,
        rating: true,
        movieAndYear: true,
        characters: {
          select: {
            id: true,
            name: true,
            image: true,
          },
        },
      },
    });

    return movie;
  }
  async getAll(
    name?: string,
    order?: string,
    year?: number,
  ): Promise<MovieWithCharacters[]> {
    console.log(name, order, year);

    const orderBy =
      order === 'asc' ? Prisma.SortOrder.asc : Prisma.SortOrder.desc;

    return await this.prismaService.movie.findMany({
      where: {
        deletedAt: null,

        //Si la variable name existe, entonces se agrega el filtro
        title: {
          contains: name || undefined,
        },
        year: year || undefined,

        // //Si la variable name existe, entonces se agrega el filtro
        // ...(name && {
        //   title: {
        //     contains: name,
        //   },
        // }),
      },
      orderBy: {
        title: orderBy,
      },
      select: {
        id: true,
        title: true,
        image: true,
        year: true,
        characters: {
          select: {
            id: true,
            name: true,
            image: true,
          },
        },
      },
    });
  }

  async create(movie: CreateMovieDto) {
    const newMovie = await this.prismaService.movie.create({
      data: {
        title: movie.title,
        image: movie.image,
        year: movie.year,
        rating: movie.rating,
      },
      select: {
        id: true,
        title: true,
        image: true,
        year: true,
        rating: true,
      },
    });
    return newMovie;
  }

  async update(id: number, changes: CreateMovieDto) {
    const findMovie = await this.prismaService.movie.findUnique({
      where: {
        id: id,
      },
    });

    if (!findMovie) {
      throw new NotFoundException('Movie not found');
    }

    if (findMovie.deletedAt) {
      throw new ConflictException('Movie deleted');
    }

    const updateMovie = await this.prismaService.movie.update({
      where: {
        id: id,
      },
      data: {
        ...changes,
      },
      select: {
        id: true,
        title: true,
        image: true,
        year: true,
        rating: true,
      },
    });
    return updateMovie;
  }

  async delete(id: number) {
    const findMovie = await this.prismaService.movie.findUnique({
      where: {
        id: id,
      },
    });

    if (!findMovie || findMovie.deletedAt) {
      throw new NotFoundException('Movie not found');
    }
    await this.prismaService.movie.update({
      where: {
        id: id,
      },
      data: {
        deletedAt: new Date(),
      },
    });
    return findMovie;
  }
}
