import { Prisma, PrismaClient } from '@prisma/client';

export const computeMovieYear = new PrismaClient().$extends({
  result: {
    movie: {
      movieAndYear: {
        needs: { title: true, year: true },
        compute(movie) {
          return `${movie.title} (${movie.year})`;
        },
      },
    },
  },
});

const MovieWithCharacters = Prisma.validator<Prisma.MovieDefaultArgs>()({
  select: {
    id: true,
    title: true,
    image: true,
    year: true,
    characters: {
      select: {
        id: true,
        name: true,
        image: true,
      },
    },
  },
});

export type MovieWithCharacters = Prisma.MovieGetPayload<
  typeof MovieWithCharacters
>;
