import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { MovieService } from './movie.service';
import { CreateMovieDto, QueryMovieDto } from './dto/movie.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
//// import { ApiBody } from '@nestjs/swagger';
@ApiTags('Movie')
@Controller('movie')
export class MovieController {
  constructor(private movieService: MovieService) {}

  /**
   *   Obtiene todas las peliculas
   */
  @Get()
  @ApiBearerAuth()
  getMovies(@Query() query: QueryMovieDto) {
    return this.movieService.getAll(query.name, query.order, query.year);
  }
  @Get(':id')
  getMovieById(@Param('id') id: number) {
    return this.movieService.getOne(id);
  }
  @Post()
  // @ApiBody({ type: CreateMovieDto })
  async createMovie(@Body() movie: CreateMovieDto) {
    return await this.movieService.create(movie);
  }
  @Patch(':id')
  async updateMovie(@Param('id') id: number, @Body() payload: CreateMovieDto) {
    return await this.movieService.update(id, payload);
  }

  @Delete(':id')
  @HttpCode(204)
  async deleteMovie(@Param('id') id: number) {
    return this.movieService.delete(id);
  }
}
