import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
export class CreateMovieDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  image: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  title: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  @Min(1900)
  @Max(2021)
  year?: number;

  @IsOptional()
  @IsNumber()
  @ApiProperty()
  @Min(0.1)
  @Max(5)
  rating?: number;
}

export class QueryMovieDto {
  @IsOptional()
  @IsString()
  @ApiProperty()
  name?: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  order?: OrderBy;

  @IsOptional()
  @IsNumber()
  @ApiProperty()
  year?: number;
}

export enum OrderBy {
  ASC = 'asc',
  DESC = 'desc',
}
