import { User } from '@prisma/client';

export interface ValidatedUser {
  user: User;
}
