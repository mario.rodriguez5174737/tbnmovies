import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { PERFIL_KEY } from '../decorator';
import { Perfil } from '../enum';

@Injectable()
export class PerfilesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const perfilRequerido = this.reflector.getAllAndOverride<Perfil[]>(
      PERFIL_KEY,
      [context.getHandler(), context.getClass()],
    );

    if (!perfilRequerido) {
      return true;
    }

    const { user } = context.switchToHttp().getRequest();
    return perfilRequerido.some((perfil) => user.role == perfil);
  }
}
