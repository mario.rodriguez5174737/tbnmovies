import { SetMetadata } from '@nestjs/common';
import { Perfil } from '../enum';

export const PERFIL_KEY = 'perfiles';
export const Perfiles = (...perfiles: Perfil[]) =>
  SetMetadata(PERFIL_KEY, perfiles);
