import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AuthDto } from './dto/auth.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import * as argon from 'argon2';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private prismaService: PrismaService,
    private jwtService: JwtService,
    private config: ConfigService,
  ) {}
  async register(payload: AuthDto) {
    const hashedPassword = await argon.hash(payload.password);
    const emailExists = await this.prismaService.user.findUnique({
      where: {
        email: payload.email,
      },
    });
    if (emailExists) {
      throw new ConflictException('Email already exists');
    }
    const newUser = await this.prismaService.user.create({
      data: {
        email: payload.email,
        password: hashedPassword,
      },
      select: {
        id: true,
        email: true,
      },
    });
    return newUser;
  }
  async login(payload: AuthDto) {
    const user = await this.prismaService.user.findUnique({
      where: {
        email: payload.email,
      },
    });
    if (!user) {
      throw new NotFoundException('User or password incorrect');
    }
    const passwordValid = await argon.verify(user.password, payload.password);
    if (!passwordValid) {
      throw new NotFoundException('User or password incorrect');
    }
    delete user.password;
    const token = await this.signToken(user.id, user.role);
    return { token, user };
  }

  private signToken(userId: number, role: string): Promise<string> {
    const data = { sub: userId, role };
    const secret = this.config.get('JWT_SECRET');
    return this.jwtService.signAsync(data, { expiresIn: '1d', secret: secret });
  }
}
