import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthDto } from './dto/auth.dto';
import { AuthService } from './auth.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from './decorator/getUser.decorator';
import { ValidatedUser } from './type/auth.type';
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('register')
  async register(@Body() payload: AuthDto) {
    return await this.authService.register(payload);
  }
  @Post('login')
  async login(@Body() payload: AuthDto) {
    return await this.authService.login(payload);
  }

  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @Get('me')
  getMe(@GetUser() user: ValidatedUser) {
    return user;
  }
}
