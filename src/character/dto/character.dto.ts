import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateCharacterDto {
  @IsNotEmpty()
  @IsString()
  name: string;
  @IsNotEmpty()
  @IsNumber()
  age: number;
  @IsNotEmpty()
  @IsNumber()
  weight: number;

  @IsNotEmpty()
  @IsString()
  image: string;

  @IsOptional()
  @IsString()
  story?: string;
  @IsNotEmpty()
  @IsNumber()
  movieId: number;
}

export class QueryCharacterDto {
  @IsOptional()
  @IsNumber()
  @ApiProperty()
  movieId?: number;
}
