import { ConflictException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateCharacterDto } from './dto/character.dto';

@Injectable()
export class CharacterService {
  constructor(private prismaService: PrismaService) {}

  async findAll(movieId?: number) {
    const characters = await this.prismaService.character.findMany({
      where: {
        movieId: movieId || undefined,
      },
      select: {
        id: true,
        name: true,
        image: true,
        movie: {
          select: {
            id: true,
            title: true,
            image: true,
          },
        },
      },
    });
    return characters;
  }
  async create(character: CreateCharacterDto) {
    const movieExist = await this.prismaService.movie.findUnique({
      where: {
        id: character.movieId,
      },
    });
    if (!movieExist) {
      throw new ConflictException('Movie does not exist');
    }

    const newCharacter = await this.prismaService.character.create({
      data: {
        name: character.name,
        image: character.image,
        age: character.age,
        weight: character.weight,
        story: character.story,
        movieId: character.movieId,
        /*       
         * Este fragmento de código es equivalente a la linea de arriba
         ? Diferencias ???
        movie: {
          connect: {
            id: character.movieId,
          },
        }, */
      },
      select: {
        id: true,
        name: true,
        image: true,
        movie: {
          select: {
            id: true,
            title: true,
            image: true,
          },
        },
      },
    });
    return newCharacter;
  }
}
