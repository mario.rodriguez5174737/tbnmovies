import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { CharacterService } from './character.service';
import { CreateCharacterDto, QueryCharacterDto } from './dto/character.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Perfiles } from 'src/auth/decorator';
import { Perfil } from 'src/auth/enum';
import { PerfilesGuard } from 'src/auth/guard/perfiles.guard';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Character')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('character')
export class CharacterController {
  constructor(private characterService: CharacterService) {}

  @Get()
  async getAll(@Query() query: QueryCharacterDto) {
    return this.characterService.findAll(query.movieId);
  }
  @Post()
  @Perfiles(Perfil.ADMIN)
  @UseGuards(PerfilesGuard)
  async create(@Body() payload: CreateCharacterDto) {
    return this.characterService.create(payload);
  }
}
