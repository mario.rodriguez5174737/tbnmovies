import { Module } from '@nestjs/common';
import { MovieModule } from './movie/movie.module';
import { AuthModule } from './auth/auth.module';
import { PrismaModule } from './prisma/prisma.module';
import { CharacterModule } from './character/character.module';
import { ConfigModule } from '@nestjs/config';

//Esto es una prueba x
@Module({
  imports: [
    MovieModule,
    AuthModule,
    PrismaModule,
    CharacterModule,
    ConfigModule.forRoot({
      isGlobal: true,
      ignoreEnvFile: true,
    }),
  ],
})
export class AppModule {}
